package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class SampleController implements Initializable{
	
	@FXML TextField nombre;
	@FXML TextField apellido;
	@FXML Button enviar;
	@FXML Label datos;


	@FXML
	public void enviarDatos(ActionEvent accion) {
		String nombreuno = nombre.getText();
		String apellidouno = apellido.getText();
		datos.setText("Tu nombre y apellidos " + nombreuno + " " +apellidouno);
	}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
}
